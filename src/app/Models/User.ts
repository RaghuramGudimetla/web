export class User {

    // Fields
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    gender: string ;
    isMarried: boolean;
    country: string;

}
