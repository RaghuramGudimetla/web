import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

// Components, Pipes
import { AppComponent } from './app.component';
import { LoginComponentComponent } from './Components/login-component/login-component.component';
import { RegisterComponent } from './Components/register/register.component';
import { ImageUrlPipe } from '../app/Pipes/image-url.pipe';
import { AlertModalComponent } from './Components/alert-modal/alert-modal.component';
import { GridviewComponent } from './Components/gridview/gridview.component';

// Services
import { InterceptorService } from '../app/Services/interceptor.service';
import { UserService } from '../app/Services/user.service';
import { PreferencesService } from '../app/Services/preferences.service';
import { HighchartComponent } from './Components/highcharts/highchart/highchart.component';
import { HighchartbarComponent } from './Components/highcharts/highchartbar/highchartbar.component';
import { HighchartpieComponent } from './Components/highcharts/highchartpie/highchartpie.component';
import { HighchartareaComponent } from './Components/highcharts/highchartarea/highchartarea.component';
import { HomeScreenComponent } from './Components/ipl/home-screen/home-screen.component';
import { IplCardComponent } from './Components/ipl/ipl-card/ipl-card.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponentComponent,
    RegisterComponent,
    ImageUrlPipe,
    AlertModalComponent,
    GridviewComponent,
    HighchartComponent,
    HighchartbarComponent,
    HighchartpieComponent,
    HighchartareaComponent,
    HomeScreenComponent,
    IplCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true},
    UserService,
    PreferencesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
