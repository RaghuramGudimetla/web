import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importing Components for Routes
import { LoginComponentComponent } from './Components/login-component/login-component.component';
import { HighchartComponent } from './Components//highcharts/highchart/highchart.component';
import { HighchartbarComponent } from './Components/highcharts/highchartbar/highchartbar.component';
import { GridviewComponent } from './Components/gridview/gridview.component';
import { HomeScreenComponent } from './Components/ipl/home-screen/home-screen.component'

// Defining Routes
const routes: Routes = [

  { path: '',  redirectTo: '/iplhome', pathMatch: 'full' },
  { path: 'login', component: LoginComponentComponent },
  { path: 'highchart', component: HighchartComponent},
  { path: 'highchartbar', component: HighchartbarComponent},
  { path: 'gridview', component: GridviewComponent},
  { path: 'iplhome', component: HomeScreenComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
