import { Component, OnInit } from '@angular/core';

// Importing Data
import { GRIDDATA } from '../../Common/data';

@Component({
  selector: 'app-gridview',
  templateUrl: './gridview.component.html',
  styleUrls: ['./gridview.component.scss']
})
export class GridviewComponent implements OnInit {


  public gridData = []

  constructor() { }

  ngOnInit(): void {

    this.gridData = GRIDDATA;

  }

}
