import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { BARDATA } from '../../../Common/data';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-highchartbar',
  templateUrl: './highchartbar.component.html',
  styleUrls: ['./highchartbar.component.sass']
})
export class HighchartbarComponent implements OnInit {

  // Get the chart options
  public options: any;

  constructor() { }

  ngOnInit(): void {

    this.options = BARDATA;

    // Initialize the chat container
    Highcharts.chart('barContainer', this.options);

  }

}
