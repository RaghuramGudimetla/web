import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighchartbarComponent } from './highchartbar.component';

describe('HighchartbarComponent', () => {
  let component: HighchartbarComponent;
  let fixture: ComponentFixture<HighchartbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighchartbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighchartbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
