import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighchartpieComponent } from './highchartpie.component';

describe('HighchartpieComponent', () => {
  let component: HighchartpieComponent;
  let fixture: ComponentFixture<HighchartpieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighchartpieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighchartpieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
