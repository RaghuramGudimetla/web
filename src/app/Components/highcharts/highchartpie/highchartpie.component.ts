import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { PIEDATA } from '../../../Common/data';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-highchartpie',
  templateUrl: './highchartpie.component.html',
  styleUrls: ['./highchartpie.component.sass']
})
export class HighchartpieComponent implements OnInit {

  public options: any;

  constructor() { }

  ngOnInit(): void {

    this.options = PIEDATA;

    Highcharts.chart('pieContainer', this.options);

  }

}
