import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { AREADATA } from '../../../Common/data';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-highchartarea',
  templateUrl: './highchartarea.component.html',
  styleUrls: ['./highchartarea.component.sass']
})
export class HighchartareaComponent implements OnInit {

  // Get the chart options
  public options: any;

  constructor() { }

  ngOnInit(): void {

    this.options = AREADATA;

    // Initialize chart container
    Highcharts.chart('areaContainer', this.options);

  }

}
