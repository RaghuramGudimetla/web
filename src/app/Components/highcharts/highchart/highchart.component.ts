import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { SCATTERDATA } from '../../../Common/data';
import { UserService } from '../../../Services/user.service';
import { Observable } from 'rxjs';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-highchart',
  templateUrl: './highchart.component.html',
  styleUrls: ['./highchart.component.sass']
})
export class HighchartComponent implements OnInit {

  // Observable to update the chart
  public apiObservable: Observable<any>;

  // Get the chart options
  public options: any;

  constructor(private userService: UserService) { }

  ngOnInit() {

    this.options = SCATTERDATA;
    Highcharts.chart('scatterContainer', this.options);
    this.apiObservable = this.userService.highScatterData();

    setInterval(() => {
      this.updateData();
    }, 10000);

  }

  /*
    Update the data in chart
  */
  public updateData(): void {

    this.apiObservable.subscribe((response) => {
      if (response) {
        const updatedNormalData = [];
        const updatedAbnormalData = [];
        response.forEach(row => {
          const TEMPROW = [
            new Date(row.timestamp).getTime(),
            row.value
          ];
          row.Normal === 1 ? updatedNormalData.push(TEMPROW) : updatedAbnormalData.push(TEMPROW);
        });
        this.options.series[0].data = updatedNormalData;
        this.options.series[1].data = updatedAbnormalData;
        Highcharts.chart('container', this.options);
      }
    }, (error) => {
      if (error) {
        this.userService.error(error);
      }
    });
  }

}
