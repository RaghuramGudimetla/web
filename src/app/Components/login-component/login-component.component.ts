import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../Models/User';
import { UserService } from '../../Services/user.service';
import { PreferencesService } from '../../Services/preferences.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.scss']
})
export class LoginComponentComponent implements OnInit {

  // Regster form object
  public registerForm = new FormGroup({});

  // Login form object
  public loginForm = new FormGroup({});

  // Countries List
  public countryList: string[] = [];

  // Login value
  public isLogin = true;

  // Success
  public isSuccess = false;

  private user: User;

  // Modal Variables
  public modalTitle = '';
  public modalFooter = '';
  public modalBody = '';

  constructor(private userService: UserService,
              private preferencesService: PreferencesService,
              private router: Router ) { }

  ngOnInit(): void {

    this.countryList = ['India', 'NZ'];

    this.registerForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.minLength(5)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(5)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      gender: new FormControl('male'),
      isMarried: new FormControl(true),
      country: new FormControl()
    });

    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });

  }

  /*
    Method for registration submit button
  */
  public onRegisterSubmit(): void {

    if (this.registerForm.valid) {

      this.user = { ...this.user, ...this.registerForm.value };

      this.userService.registerUser(this.user).subscribe((response) => {
        if (response) {
          this.registerForm.reset();
        }
      });

    }

  }

  /*
    Method for login submit button
  */
  public onLoginSubmit(): void {

    if (this.loginForm.valid) {

      this.loginForm.reset();

      this.userService.loginUser(this.loginForm.value).subscribe((response) => {
        if (response) {

          this.preferencesService.setToken(response.token);
          this.router.navigateByUrl('/imageview');

        }
      });

    }

  }

  /*
    Fetch user token
  */
  public register(event: any): void {
    this.isLogin = !this.isLogin;
    console.log(this.isLogin);
  }

}
