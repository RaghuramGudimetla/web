import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { IplDataService } from '../../../Services/ipl-data.service';

@Component({
  selector: 'app-ipl-card',
  templateUrl: './ipl-card.component.html',
  styleUrls: ['./ipl-card.component.scss']
})
export class IplCardComponent implements OnInit, OnChanges {

  @Input() card: any; 

  @Input() currentTeam: String;

  public imageUrl: String;

  public cardData: [];

  public colors: String[];

  constructor(private iplDataService: IplDataService) { 

    this.imageUrl = '';

    this.cardData = [];

    this.colors = [
      'rgb(201, 198, 25)',
      'rgb(156, 160, 160)',
      'rgb(182, 141, 7)'
    ]
  }

  ngOnInit(): void {
    
    this.imageUrl = '../../../../assets/images/' + this.card['link'] + '.jpg';

  }

  ngOnChanges(): void {

    this.getData();

  }

  /*
     Method for registration submit button
   */
  public getData(): void {

    let url = this.card['link'] + '/' + this.currentTeam

    this.iplDataService.getData(url).subscribe((response) => {
            
      if (response && response.result) {

        let data = response.result;

        this.cardData = data;
        
      }

    });

  }

}
