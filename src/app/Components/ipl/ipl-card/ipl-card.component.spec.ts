import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IplCardComponent } from './ipl-card.component';

describe('IplCardComponent', () => {
  let component: IplCardComponent;
  let fixture: ComponentFixture<IplCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IplCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IplCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
