import { Component, OnInit } from '@angular/core';
import { IplDataService } from '../../../Services/ipl-data.service';

@Component({
  selector: 'app-home-screen',
  templateUrl: './home-screen.component.html',
  styleUrls: ['./home-screen.component.scss']
})
export class HomeScreenComponent implements OnInit {

  public teams: String[];

  public currentTeam: String;

  public cardsData;

  constructor(private iplDataService: IplDataService) {

    this.teams = [];

    this.currentTeam = 'All';

    this.cardsData = [
      {
        cardName: 'Man of the Match',
        link: 'moms',
      },
      {
        cardName: 'Most Runs',
        link: 'runs',
      },
      {
        cardName: 'Most Wickets',
        link: 'wickets',
      }
    ];

  }

  ngOnInit(): void {

    this.getTeams();

    this.screenUpdate();

  }

  public screenUpdate(): void {

    

  }

  /*
     Method for registration submit button
   */
  public getTeams(): void {

    this.iplDataService.getTeams().subscribe((response) => {

      if (response && response.result) {

        let allTeams = response.result;

        allTeams.map((value) => {

          this.teams.push(value['winner']);

        });

      }

    });

  }

  public changeTeam(event: any) {

    if (this.currentTeam !== event.target.value) {

      this.currentTeam = event.target.value;

      this.screenUpdate();

    }

  }

}
