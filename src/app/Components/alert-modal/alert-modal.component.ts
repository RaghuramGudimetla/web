import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert-modal.component.html',
  styleUrls: ['./alert-modal.component.sass']
})
export class AlertModalComponent implements OnInit {

  @Input() modalBody: string;
  @Input() modalTitle: string;
  @Input() modalFooter: string;

  constructor() { }

  ngOnInit(): void {
  }

}
