import * as Highcharts from 'highcharts';

// Scatter plot Data
export const SCATTERDATA = {
   chart: {
      type: 'scatter',
      height: 400,
      width: 500
   },
   title: {
      text: 'Scatter Plot Example'
   },
   credits: {
      enabled: false
   },
   tooltip: {
      formatter: function () {
         return 'x: ' + Highcharts.dateFormat('%e %b %y %H:%M:%S', this.x) + ' y: ' + this.y.toFixed(2);
      }
   },
   xAxis: {
      type: 'datetime',
      labels: {
         formatter: function () {
            return Highcharts.dateFormat('%e %b %y', this.value);
         }
      }
   },
   series: [
      {
         name: 'Normal',
         turboThreshold: 500000,
         data: [[new Date('2018-01-25 18:38:31').getTime(), 2],
         [new Date('2018-02-05 17:38:31').getTime(), 3],
         [new Date('2018-02-02 18:38:31').getTime(), 7],
         [new Date('2018-02-01 18:38:31').getTime(), 5]
         ]
      },
      {
         name: 'Abnormal',
         turboThreshold: 500000,
         data: [[new Date('2018-01-27 18:38:31').getTime(), 7],
         [new Date('2018-02-05 17:38:31').getTime(), 2],
         [new Date('2018-01-28 18:38:31').getTime(), 3],
         [new Date('2018-01-31 18:38:31').getTime(), 5]
         ]
      }
   ]
};

// Bar plot data
export const BARDATA = {
   chart: {
      type: 'bar',
      height: 400,
      width: 500
   },
   title: {
      text: 'Bar Plot Example'
   },
   legend: {
      layout: 'vertical',
      align: 'left',
      verticalAlign: 'top',
      x: 250,
      y: 100,
      floating: true,
      borderWidth:1,
      backgroundColor: (
         (Highcharts.theme) ||
         '#FFFFFF'), shadow: true
   },
   xAxis: {
      categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'], title: {
         text: null
      }
   },
   yAxis: {
      min: 0, title: {
         text: 'Population (millions)', align: 'high'
      },
      labels: {
         overflow: 'justify'
      }
   },
   tooltip: {
      valueSuffix: ' millions'
   },
   plotOptions: {
      bar: {
         dataLabels: {
            enabled: true
         }
      }
   },
   credits: {
      enabled: false
   },
   series: [
      {
         name: 'Year 1800',
         data: [107, 31, 635, 203, 2]
      },
      {
         name: 'Year 1900',
         data: [133, 156, 947, 408, 6]
      },
      {
         name: 'Year 2008',
         data: [973, 914, 4054, 732, 34]
      }
   ]
};

// Pie chart data
export const PIEDATA = {
   chart: {
      plotBorderWidth: null,
      plotShadow: false,
      height: 400,
      width: 500
   },
   title: {
      text: 'Pie Chart Example'
   },
   tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   },
   plotOptions: {
      pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         dataLabels: {
            enabled: true,
            format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
            style: {
               color: (Highcharts.theme) ||
                  'black'
            }
         }
      }
   },
   series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
         ['Firefox', 45.0],
         ['IE', 26.8],
         {
            name: 'Chrome',
            y: 12.8,
            sliced: true,
            selected: true
         },
         ['Safari', 8.5],
         ['Opera', 6.2],
         ['Others', 0.7]
      ]
   }]
};

// Area chart data
export const AREADATA = {
   chart: {
      type: 'area',
      height: 400,
      width: 500
   },
   title: {
      text: 'Area Plot'
   },
   xAxis: {
      categories: ['1750', '1800', '1850', '1900', '1950', '1999', '2050'],
      tickmarkPlacement: 'on',
      title: {
         enabled: false
      }
   },
   yAxis: {
      title: {
         text: 'Billions'
      },
      labels: {
         formatter: function () {
            return this.value / 1000;
         }
      }
   },
   tooltip: {
      shared: true,
      valueSuffix: ' millions'
   },
   plotOptions: {
      area: {
         stacking: 'normal',
         lineColor: '#666666',
         lineWidth: 1,

         marker: {
            lineWidth: 1,
            lineColor: '#666666'
         }
      }
   },
   credits: {
      enabled: false
   },
   series: [
      {
         name: 'Asia',
         data: [502, 635, 809, 947, 1402, 3634, 5268]
      },
      {
         name: 'Africa',
         data: [106, 107, 111, 133, 221, 767, 1766]
      },
      {
         name: 'Europe',
         data: [163, 203, 276, 408, 547, 729, 628]
      },
      {
         name: 'America',
         data: [18, 31, 54, 156, 339, 818, 1201]
      },
      {
         name: 'Oceania',
         data: [2, 2, 2, 6, 13, 30, 46]
      }
   ]
};

// Data for Grid
export const GRIDDATA = [
   { 
      date: '24-01-2021',
      site: 'Chc',
      duration: 30,
      status: 'Active',
      claimNo: 123,
      exam: '345',
      charge: 50,
      invoiced: 40,
      paidwo: 10
   },
   { 
      date: '24-01-2021',
      site: 'Chc',
      duration: 30,
      status: 'Active',
      claimNo: 123,
      exam: '345',
      charge: 50,
      invoiced: 40,
      paidwo: 10
   },
   { 
      date: '24-01-2021',
      site: 'Chc',
      duration: 30,
      status: 'Active',
      claimNo: 123,
      exam: '345',
      charge: 50,
      invoiced: 40,
      paidwo: 10
   },
   { 
      date: '24-01-2021',
      site: 'Chc',
      duration: 30,
      status: 'Active',
      claimNo: 123,
      exam: '345',
      charge: 50,
      invoiced: 40,
      paidwo: 10
   },
   { 
      date: '24-01-2021',
      site: 'Chc',
      duration: 30,
      status: 'Active',
      claimNo: 123,
      exam: '345',
      charge: 50,
      invoiced: 40,
      paidwo: 10
   }
];