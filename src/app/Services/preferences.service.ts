import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/*
  Service to save user preferences
*/
export class PreferencesService {

  // Token Variable
  private token: string;

  // country
  private country: string;

  constructor() { }

  /*
    Fetch user token
  */
  public getToken(): string {
    return this.token;
  }

  /*
    Fetch user token
  */
  public getCountry(): string {
    return this.country;
  }

  /*
    Set user token
  */
  public setToken(token: string): void {
    this.token = token;
  }

  /*
    Set user country
  */
  public setCountry(country: string): void {
    this.country = country;
  }

}
