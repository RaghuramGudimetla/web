import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IplDataService {

  public apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = 'http://127.0.0.1:5000/';
  }

  /* Service to register users
     @user - User details
  */
  public getTeams(): Observable<any> {

    const url = this.apiUrl + 'teams';

    return this.http.get(url).pipe(tap(data => console.log(data)),
      catchError(this.error)
    );

  }

  /* Service to register users
     @user - User details
  */
  public getMoms(resource: String): Observable<any> {

    const url = this.apiUrl + 'moms/' + resource;

    return this.http.get(url).pipe(tap(data => console.log(data)),
      catchError(this.error)
    );

  }

  /* Service to register users
       @user - User details
    */
  public getData(resource: String): Observable<any> {

    const url = this.apiUrl + resource;

    return this.http.get(url).pipe(tap(data => console.log(data)),
      catchError(this.error)
    );

  }

  // Method to handle HTTP errors
  public error(err: HttpErrorResponse) {

    let errorMessage = '';

    if (err.error instanceof ErrorEvent) {
      errorMessage = err.error.message;
    } else {
      errorMessage = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }

    return throwError(errorMessage);

  }

}
