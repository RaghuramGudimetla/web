import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpResponse, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { PreferencesService } from '../../app/Services/preferences.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private preferencesService: PreferencesService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // All HTTP requests are going to go through this method

    // Retrieve the token
    const TOKEN = this.preferencesService.getToken();
    let newHeaders = req.headers;
    if (TOKEN) {
      newHeaders = newHeaders.append('Authorization', `Bearer ${TOKEN}`);
    }
    const authReq = req.clone({ headers: newHeaders });
    return next.handle(authReq);
  }

}
