import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public url: string;
  public apiUrl: string;
  public highChartURL: string;

  constructor(private http: HttpClient) {

    this.url = 'http://localhost:3000/';
    this.apiUrl = 'https://api.apis.guru/v2/list.json';
    this.highChartURL = '';

  }

  /* Service to register users
     @user - User details
  */
  public registerUser(user: any): Observable<any> {

    const url = this.url + 'user/register';

    return this.http.post(url, user, {responseType: 'text'}).pipe(tap( data => console.log(data)),
      catchError(this.error)
    );

  }

  /* Service to Verify User login
     @ user - User Data
  */
  public loginUser(user: any): Observable<any> {

    const url = this.url + 'user/login';
    return this.http.post(url, user, {responseType: 'json'}).pipe(
      tap( data => console.log(data)),
      catchError(this.error)
    );

  }

  /* Service to verify user login
     @ user - User Data
  */
  public imageData(): Observable<any> {

    return this.http.get(this.apiUrl).pipe(
      tap( data => console.log(data)),
      catchError(this.error)
    );

  }

  public highScatterData(): Observable<any> {
    return this.http.get(this.highChartURL, {}).pipe(
      tap( data => console.log(data) ),
      catchError(this.error)
    );
  }

  // Method to handle HTTP errors
  public error(err: HttpErrorResponse) {

    let errorMessage = '';

    if (err.error instanceof ErrorEvent) {
      errorMessage = err.error.message;
    } else {
      errorMessage = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }

    return throwError(errorMessage);

  }

}
